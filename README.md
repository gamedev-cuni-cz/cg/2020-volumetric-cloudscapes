# 2020 - Volumetric Cloudscapes


Project focusing on implementing volumetric clouds sitting inside atmosphere on a spherical cap.

### The framework

The cloudscapes were implemented into a DirectX framework provided to us by the lecturers
of the computer graphics for game development course. It is based on the texturing example
from the framework and the objects from the example are still present, to give the world
ground and sense of scale.

### Features

Features of the project need to be edited inside the source code since there is no really
good way of exposing parameters out of the application.


These features are:
1. Changing the weather map used by the system. You can change this in the source file
`clouds.cpp` *line 73*. The weather map is loaded from the disk as a dds texture file.
There are several examples which are written in the comment next to the loading call.
2. Changing the colour and direction of the sun. You can change this in the source file
`clouds.cpp` *lines 268, 270*. These parameters are two `float4` vectors.

### Performance

The application can be quite sluggish but I get 30-60 FPS on the example since with my
GTX 1060 so it shouldn't be too bad. However, if it seems to be very slow then you can
change one(or both) of
- Ray marching steps. It is in `CloudsSpherer.fx` *line 91*. There are two numbers, the first
is base amount of steps (minimal amount), the second is how many additional steps will the
application do when looking towards the horizon. The step count gradually increases towards
the horizon.
- Texture size, this is a bigger factor than ray marching steps. It is in `CloudsCommon.fx`
*line 15*. Bigger number means texture samples closer together and less detailed result, but
a performance improvement.

