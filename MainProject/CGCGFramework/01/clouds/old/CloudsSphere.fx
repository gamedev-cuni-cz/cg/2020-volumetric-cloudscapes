
#include "CloudsCommon.fx"
#include "../shaders/PhongLights.fx"

Texture3D txCloudMain : register(t0);
Texture3D txCloudDetail : register(t1);
Texture2D txStaticCoverage : register(t2);

SamplerState samLinear : register(s0);
SamplerState clampedSamLinear : register(s1);

cbuffer ConstantBuffer : register(b0) {
	matrix World;
	matrix View;
	matrix Projection;
	matrix NormalMatrix;
	DirLight DirLights[1];
	float3 ViewPos;
	int DirLightCount;
}


struct VS_INPUT_SPHERE
{
	float4 Pos : POSITION;
	float3 Norm : NORMAL;
	float2 UV : LATLONG;
};

struct PS_INPUT_SPHERE
{
	float4 Pos : SV_POSITION;
	float3 Norm : TEXCOORD1;
	float3 FragPos : POSITION;
	float2 UV : LATLONG;
};


// ====================
// Vertex Shader Sphere
// ====================
PS_INPUT_SPHERE VSSphere(VS_INPUT_SPHERE input) {
	PS_INPUT_SPHERE output = (PS_INPUT_SPHERE)0;
	output.Pos = mul(input.Pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Norm = mul(float4(input.Norm, 0), NormalMatrix).xyz;
	output.FragPos = mul(input.Pos, World);
	output.UV = input.UV;

	return output;
}


// ===================
// Pixel shader Sphere
// ===================
float4 PSSphere(PS_INPUT_SPHERE input) : SV_Target{
	float3 normal = normalize(input.Norm);
	float3 viewDir = normalize(ViewPos - input.FragPos);
	float3 outDir = -viewDir;

	float4 debugCol = float4(0, 0, 0, 1);

	float3 lightDir = normalize(-DirLights[0].Direction);
	float sunlight = pow(max(dot(outDir, lightDir), 0.0), 150);
	float4 bgCol = float4(0.1, 0.1, (0.4 + dot(viewDir, float4(0, -1, 0, 0))) / 1.4, 1.0);
	float4 sunCol = DirLights[0].Color;
	float4 skyCol = sunlight * sunCol + (1 - sunlight) * bgCol;

	float3 ambientSkyCol = skyCol.rgb / FourPI;
	float3 ambientSunCol = DirLights[0].Color / FourPI;
	float extinctionFactor = 0.04;
	//float scatteringFactor = 0.5;
	float scatteringFactor = HenyeyGreenstein(1);

	float hgDivFactor = HenyeyGreenstein(1);

	// RAY MARCH
	float4 col = float4(0, 0, 0, 0);

	float slope = (dot(outDir, float3(0, 1, 0)) + 0.5) / 1.5;

	float scale = 3000;
	int stepCount = 128;// +int(64 * (1 - stepSlope));
	float3 stepDir = outDir;
	float smallStepSize = (2500.0 / 64.0) / slope;
	float bigStepSize = (2500 / 32.0) / slope;
	float currentStepSize = bigStepSize;

	float3 top = IntersectAtmosphere(ViewPos, outDir);
	float dAtm = length(top - input.FragPos);
	float countMod = (dAtm - CloudHeight) / (AtmoshpereSizeAtHorizon - CloudHeight);
	stepCount = 64 + int(64 * countMod);
	smallStepSize = dAtm / stepCount;
	bigStepSize = smallStepSize * 2.0;
	debugCol = stepCount / 128.0;
	currentStepSize = smallStepSize;
	//currentStepSize = 100;

	float3 stepCoords = input.FragPos.xyz;
	//float rnd = (sin(0.79531 * input.FragPos.x + 4.3269 - 46.32169 * input.FragPos.z - 4.2369) + 1.0) / 2.0;
	float rnd = txCloudDetail.Sample(samLinear, float3(input.FragPos.xz / 2, 0.5));
	//debugCol = rnd;
	stepCoords += smallStepSize * rnd * 0.99;

	bool bigStep = true;
	bool inCloud = false;
	float3 cloudStart = float3(0, 0, 0);
	int outOfCloudSteps = 0;
	int inCloudSteps = 0;

	float extinction = 1;
	float3 scattering = 0;

	[loop] for (int step = 0; step < stepCount; ++step)
	{
		float altitude = GetAltitude(stepCoords);
		float normAltitude = altitude / CloudHeight;

		[branch] if (altitude > CloudHeight)
		{
			debugCol = float4(0, 1.0, 0, 1);
			break;
		}

		//float rnd = (sin(0.32648 * input.FragPos.x + 0.66324 * input.FragPos.z + 0.4365 * input.FragPos.y) + 1.0) / 2.0;
		//float rnd = 0;
		float rnd = txCloudDetail.Sample(samLinear, input.FragPos) * 0.4;
		float3 texCoordsHigh = (stepCoords.xyz + rnd * currentStepSize) / TxScaleMain; //* 0.0003;
		float4 stepValues = txCloudMain.Sample(samLinear, texCoordsHigh);

		float2 coverageCoords = (stepCoords.xz + TxCoverageOffset) / TxCoverageScale;
		float4 coverage = txStaticCoverage.Sample(clampedSamLinear, coverageCoords);

		float distMod = DistanceModifier(stepCoords);

		float bottomCoverage = (1 - saturate(normAltitude * 4.0)) * 0.5;
		//float totalCoverage = distMod + (1 - distMod) * coverage.r * 1.0;
		//float totalCoverage = min(1 - distMod, coverage.r) + bottomCoverage;
		float totalCoverage = max(distMod, coverage.r);// +bottomCoverage;
		//debugCol = coverage.r * 20;
		//break;
		float2 cloudGrad = GetCloudGradient(totalCoverage + bottomCoverage);
		//float2 cloudGrad = GetCloudGradient(distMod);
		//totalCoverage = distMod;

		//debugCol = cloudGrad.g;
		//float2 cloudGrad = GetCloudGradient(coverage.r + bottomCoverage);
		float altMod = GetAltitudeCoverage(normAltitude, cloudGrad);
		//debugCol = normAltitude;
		if (normAltitude > 0.2 && normAltitude < 0.7 && altMod < 0.01)
		{
			debugCol = float4(1, 0, 0, 1);
		}
		//altMod = clamp(altMod, cloudGrad.x, cloudGrad.y);
		//altMod = max(distMod, altMod);
		//float lowRange = lerp(0.2, 0.05, altMod);

		//float cloudSel = CloudSelector(totalCoverage, altitude);

		//float modelA = stepValues.r * 0.6 + stepValues.b * 0.3;
		//float modelB = (stepValues.r + stepValues.g) * 0.5 * stepValues.b * stepValues.a + 0.3 * (1 - normAltitude);

		//float localDensity = modelB * AltitudeModifier(altitude) * cloudSel;
		//float areClouds = totalCoverage.r > 0 ? 1.0 : 0.0;
		//float localDensity = GetAltitudeDensity(normAltitude, modelA) * cloudGrad * totalCoverage;// *totalCoverage * 2;// * coverage.r * 2;
		//float localDensity = GetAltitudeDensity(normAltitude, modelA) * altMod* totalCoverage;// *totalCoverage * 2;// * coverage.r * 2;
		//float localDensity = modelA * altMod * totalCoverage;
		//localDensity = saturate(localDensity);
		float localDensity = MainCloudShape(stepValues, normAltitude, cloudGrad, totalCoverage);
		//localDensity -= ErosionModifier(altitude - 1000) * 0.5;
		//localDensity -= stepValues.g * ErosionModifier(altitude) * 0.3;
		//float localDensity = stepValues.r * AltitudeModifier(altitude) * BaseDensityIncrease(altitude) * cloudSel;

		//float densityModel = totalCoverage * localDensity;
		float densityModel = localDensity;

		float model = densityModel;

		[flatten] if (inCloud)
		{
			inCloudSteps += 1;
			[branch] if (inCloudSteps < 5)
			{
				float4 detailValues = txCloudDetail.Sample(samLinear, texCoordsHigh * 0.5);
				//model = model * detailValues.r;
				//float erMod = 0.2;
				//float erMod = ErosionModifier(altitude) * 0.4;
				float erMod = (normAltitude + 0.5);
				//float erVal = (detailValues.r + detailValues.g) * 0.5 * detailValues.b;
				float erVal = detailValues.r * 0.6 + detailValues.b * 0.3;
				//model = saturate(model - (1 - detailValues.r) * erMod);
				//model = saturate(model - erVal * erMod);
				//model -= 1;
			}
		}

		[branch] if (model <= 0.002)
		{
			[flatten] if (inCloud)
			{
				outOfCloudSteps += 1;
			}
			stepCoords += stepDir * currentStepSize;
			continue;
		}
		[branch] if (!inCloud)
		{
			inCloud = true;
			outOfCloudSteps = 0;
			/*stepCoords = stepCoords - stepDir * currentStepSize;
			currentStepSize = smallStepSize;
			cloudStart = stepCoords;
			// We don't want this iteration to contirbute 1 to light
			stepCoords += stepDir * currentStepSize;
			continue;*/
		}
		/*[flatten] if (outOfCloudSteps >= 5)
		{
			inCloud = false;
			currentStepSize = bigStepSize * 4;
			inCloudSteps = 0;
		}*/

		//float scatteringCoeff = scatteringFactor * (1 - model);
		float extinctionCoeff = extinctionFactor * (1 - model) * 0.02;

		extinction *= exp(-extinctionCoeff * currentStepSize * 0.5);

		float powder = 1.0 - exp(-currentStepSize * (1 - model) * 0.05 * 2);

		float dist = length(stepCoords - cloudStart);
		float energy = exp(-dist / scale) * (1 - model);
		float cosAngle = dot(-stepDir, normalize(-DirLights[0].Direction));
		float HG = HenyeyGreenstein(-cosAngle);
		HG = HG / hgDivFactor;
		//float powder = 1;
		//float powder = 1.0 - exp(-dist / scale * 2);// *clamp((cosAngle + 1) / 4.0, 0, 1);

		float shade = 1;
		float3 txCoords = stepCoords / (TxScaleMain * 10);
		float3 shadeCoords = stepCoords;
		[loop] for (int i = 0; i < 6; ++i)
		{
			float offset = i == 6 ? 5 * bigStepSize : 2 * bigStepSize;
			
			shadeCoords += lightDir * offset;
			float shadeAltitude = GetAltitude(shadeCoords);
			[branch] if (shadeAltitude > CloudHeight)
			{
				break;
			}
			txCoords += (lightDir * offset) / (TxScaleMain * 10);
			float4 shadeValues = txCloudMain.Sample(samLinear, txCoords);
			shade *= /*(1 - shade)*/ exp(-offset * (1 - shadeValues.r) * 0.002);

			shadeCoords += (shadeValues.xyz - 0.5) * currentStepSize * i;
		}

		float mcol = max(max(col.r, col.g), col.b);
		float3 sunColor = sunCol.rgb;
		sunColor *= energy * HG * powder;

		//float alpha = (1 - col.a) * model + col.a;
		float alpha = (1 - extinction);

		float angle = dot(-stepDir, float3(0, -1, 0));
		//col = float4(col.rgb + /*(1 - mcol) **/ sunColor * (1 - shade), alpha);
		//col = float4(col.rgb + sunCol.rgb * shade * extinction * scatteringFactor * (1.0 - exp(-currentStepSize * extinctionCoeff * 2)), alpha);
		col = float4(col.rgb + sunCol.rgb * shade * extinction * HG * powder * 0.08, alpha);
		//col += (1-col)*float3(0.5, 0.5, 0.5) * (1 - shade) * (1 - attenuation);

		//if (col.a > 0.95)
		if (extinction < 0.05)
		{
			//debugCol = float4(1.0, 0, 0, 1);
			break;
		}

		stepCoords += stepDir * currentStepSize;
	}

	//skyCol = skyCol * (1 - col.a) + col;
	skyCol = extinction * skyCol + col;

	//return debugCol;
	return skyCol;
}
