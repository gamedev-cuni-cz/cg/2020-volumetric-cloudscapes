
static const float PI = 3.14159265f;
static const float TwoPI = 2 * PI;
static const float FourPI = 4 * PI;
static const float PI2 = PI * PI;
static const float HG_G = 0.1;
static const float CloudHeight = 2500;
static const float BaseDensityIncreaseHeight = 400;
static const float BaseAltitudeReducer = 0.5;
static const float BaseAltitudeDivider = 1 + BaseAltitudeReducer;
static const float AtmoshpereSizeAtHorizon = 13500;
static const float2 StratusGradient = float2(0.1, 0.3);
static const float2 CumulusGradient = float2(0.1, 1.0);
static const float2 CumulonimbusGradient = float2(0.75, 1.0);
static const float TxScaleMain = 2500;
static const float TxScaleDetail = 500;
static const float HorizonDensityBegin = 15000;
static const float HorizonDensityEnd = 30000;
static const float HorizonDensityDelta = HorizonDensityEnd - HorizonDensityBegin;
static const float AtmosphereRadius = 150000;
static const float TxCoverageOffset = 22000;
static const float TxCoverageScale = 44000;

static const float3 EarthCenter = float3(0, -148500, 0);

float GetAltitude(float3 pos)
{
	return length(pos - EarthCenter) - AtmosphereRadius;
}

float DistanceModifier(float3 position)
{
	float d = length(position.xz - EarthCenter.xz);
	return saturate((d - HorizonDensityBegin) / HorizonDensityDelta);
}

float SetRange(float value, float oldStart, float oldEnd, float newStart, float newEnd)
{
	return ((value - oldStart) / (oldEnd - oldStart)) * (newEnd - newStart) + newStart;
}

float StratusSelector(float alt)
{
	return smoothstep(50, 150, alt) - smoothstep(200, 300, alt);
}

float CumulusSelector(float alt)
{
	return smoothstep(50, 200, alt) - smoothstep(600, 750, alt);
}

float CumulonimbusSelector(float alt)
{
	return smoothstep(50, 500, alt) - smoothstep(1500, 2250, alt);
}

float CloudSelectorByCoverage(float coverage, float alt)
{
	float d = 200;
	alt = alt / CloudHeight;
	float max = coverage * 2000;
	float min = 0;
	float a = smoothstep(0, d, alt);
	float b = smoothstep(max - d, max, alt);
	return (a + (1 - b)) / 2;
}

float CloudSelector(float coverage, float alt)
{
	//float cn = step(0.7, coverage);
	/*float cn = smoothstep(0.5, 0.7, coverage);
	//float c = step(0.4, coverage);
	float c = smoothstep(0.2, 0.4, coverage);
	//float s = step(0.1, coverage);
	float s = smoothstep(0.05, 0.15, coverage);
	return cn * CumulonimbusSelector(alt) + (c - cn) * CumulusSelector(alt) + (s - c) * StratusSelector(alt);*/
	return CloudSelectorByCoverage(coverage, alt);
}

float AltitudeModifier(float alt)
{
	//return clamp(alt / CloudHeight, 0, 1) + (1 - clamp(alt / 300, 0, 1)) * 0.3;
	return (saturate(alt / CloudHeight) + BaseAltitudeReducer) / BaseAltitudeDivider;
}

float ErosionModifier(float alt)
{
	return saturate(alt / CloudHeight * 2);
}

float2 GetCloudGradient(float cloudType)
{
	float a = 1.0 - saturate(cloudType * 2.0);
	float b = 1.0 - abs(cloudType - 0.5) * 2.0;
	float c = saturate(cloudType - 0.5) * 2.0;

	return (StratusGradient * a) + (CumulusGradient * b) + (CumulonimbusGradient * c);
	//return (StratusGradient * a);
	//return float2(0, 0.5);
}

float GetCoverageErosion(float normAltitude, float coverage)
{
	return saturate(coverage - normAltitude * 0.1);
}

float GetAltitudeDensity(float normAltitude, float density)
{
	return saturate((normAltitude * 2 + 0.1) * density);
}

float GetAltitudeCoverage(float normAltitude, float2 gradient) {
	float coverage1 = saturate(normAltitude / 0.1);
	float coverage2 = 1.0 - saturate((normAltitude - gradient.x) / (gradient.y - gradient.x));
	//coverage2 = normAltitude < gradient.y ? 1.0 : 0.0;
	return coverage1 * coverage2;
	//return coverage1;
}

float BaseDensityIncrease(float alt)
{
	float baseAlt = saturate((BaseDensityIncreaseHeight - alt) / BaseDensityIncreaseHeight);
	return lerp(1.0, 3.0, baseAlt);
}

float HenyeyGreenstein(float cosAngle)
{
	return (1 - HG_G * HG_G) / (FourPI * pow(1 + HG_G * HG_G - 2 * HG_G * cosAngle, 3 / 2));
}

float Ei(float z) {
	return 0.5772156649015328606065 + log(1e-4 + abs(z)) + z * (1.0 + z * (0.25 + z * ((1.0 / 18.0) + z * ((1.0 / 96.0) + z * (1.0 / 600.0))))); // For z!=0
}

float3 SolveQuadratic(float a, float b, float c)
{
	float discr = b * b - 4 * a * c;
	[flatten]if (discr < 0) return float3(-1, 0, 0);
	float x0 = 0;
	[flatten] if (discr == 0)
	{
		x0 = -0.5 * b / a;
		return float3(1, x0, x0);
	}
	float x1 = 0;
	float q = (b > 0) ?
		-0.5 * (b + sqrt(discr)) :
		-0.5 * (b - sqrt(discr));
	x0 = q / a;
	x1 = c / q;

	return float3(1, max(x0, x1), min(x0, x1));
}

float3 IntersectAtmosphere(float3 start, float3 dir)
{
	float3 L = start - EarthCenter;
	float radius = (AtmosphereRadius + CloudHeight);
	float a = dot(dir, dir);
	float b = 2 * dot(dir, L);
	float c = dot(L, L) - radius * radius;
	float3 res = SolveQuadratic(a, b, c);
	return start + res.y * dir;
	/*float heightSub = start.y + EarthCenter.y;
	float coeff1 = dir.x * dir.x + dir.y * dir.y + dir.z * dir.z;
	float coeff2 = 2 * (start.x * dir.x + heightSub * dir.y + start.z * dir.z);
	float coeff3_delta = start.x * start.x + heightSub * heightSub + start.z * start.z;
	//float coeff3 = coeff3_delta - AtmosphereRadius * AtmosphereRadius;
	float coeff3 = coeff3_delta - (AtmosphereRadius + CloudHeight) * (AtmosphereRadius + CloudHeight);

	float sqrtD = sqrt(coeff2 * coeff2 - 4 * coeff1 * coeff3);
	float tpos = (-coeff2 + sqrtD) / (2 * coeff1);

	return start + tpos * dir;*/
}

float MainCloudShape(float4 noiseTex, float normAltitude, float2 gradient, float coverage)
{
	
	float altMod = GetAltitudeCoverage(normAltitude, gradient);

	//float cloudSel = CloudSelector(coverage, altitude);

	float modelA = noiseTex.r * 0.6 + noiseTex.b * 0.3;

	float modelC = saturate(SetRange(modelA, 0.0, 1.0, noiseTex.b, 1.0));
	//float modelB = (noiseTex.r + noiseTex.g) * 0.5 * noiseTex.b * noiseTex.a + 0.3 * (1 - normAltitude);
	float cwc = saturate(SetRange(modelA, coverage, 1.0, 0.0, 1.0));
	coverage = pow(coverage, SetRange(normAltitude, 0.7, 0.8, 1.0, lerp(1.0, 0.5, 0.05)));

	//float localDensity = modelB * AltitudeModifier(altitude) * cloudSel;
	float areClouds = coverage.r > 0 ? 1.0 : 0.0;
	//float localDensity = GetAltitudeDensity(normAltitude, modelA) * cloudGrad * totalCoverage;// *totalCoverage * 2;// * coverage.r * 2;
	float localDensity = GetAltitudeDensity(normAltitude, modelC) * altMod * coverage;// *totalCoverage * 2;// * coverage.r * 2;
	//float localDensity = modelA * altMod * totalCoverage;
	localDensity = saturate(localDensity);
	//localDensity = GetAltitudeDensity(normAltitude, modelA) * coverage;

	return localDensity;
}
