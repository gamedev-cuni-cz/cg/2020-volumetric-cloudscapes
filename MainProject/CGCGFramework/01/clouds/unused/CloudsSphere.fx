
#include "CloudsCommon.fx"
#include "../shaders/PhongLights.fx"

Texture3D txCloudMain : register(t0);
Texture3D txCloudDetail : register(t1);
Texture2D txStaticCoverage : register(t2);

SamplerState samLinear : register(s0);
SamplerState clampedSamLinear : register(s1);

cbuffer ConstantBuffer : register(b0) {
	matrix World;
	matrix View;
	matrix Projection;
	matrix NormalMatrix;
	DirLight DirLights[1];
	float3 ViewPos;
	int DirLightCount;
	float Time;
	float3 WindDir;
	float WindSpeed;
}


struct VS_INPUT_SPHERE
{
	float4 Pos : POSITION;
	float3 Norm : NORMAL;
	float2 UV : LATLONG;
};

struct PS_INPUT_SPHERE
{
	float4 Pos : SV_POSITION;
	float3 Norm : TEXCOORD1;
	float3 FragPos : POSITION;
	float2 UV : LATLONG;
};


// ====================
// Vertex Shader Sphere
// ====================
PS_INPUT_SPHERE VSSphere(VS_INPUT_SPHERE input) {
	PS_INPUT_SPHERE output = (PS_INPUT_SPHERE)0;
	output.Pos = mul(input.Pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Norm = mul(float4(input.Norm, 0), NormalMatrix).xyz;
	output.FragPos = mul(input.Pos, World);
	output.UV = input.UV;

	return output;
}


// ===================
// Pixel shader Sphere
// ===================
float4 PSSphere(PS_INPUT_SPHERE input) : SV_Target{
	float3 normal = normalize(input.Norm);
	float3 viewDir = normalize(ViewPos - input.FragPos);
	float3 outDir = -viewDir;

	float4 debugCol = float4(0, 0, 0, 1);

	float3 sunPos = normalize(DirLights[0].Direction) * SunDistance;

	//float3 lightDir = normalize(-DirLights[0].Direction);
	float3 lightDir = normalize(input.FragPos - sunPos);
	float sunlight = pow(max(dot(outDir, lightDir), 0.0), 150);
	float4 bgCol = float4(0.1, 0.1, (0.4 + dot(viewDir, float4(0, -1, 0, 0))) / 1.4, 1.0);
	float4 sunCol = DirLights[0].Color;
	float4 skyCol = sunlight * sunCol + (1 - sunlight) * bgCol;

	float3 ambientSkyCol = skyCol.rgb / FourPI;
	float3 ambientSunCol = DirLights[0].Color / FourPI;
	//float extinctionFactor = 0.04;
	float extinctionFactor = 0.02;
	//float scatteringFactor = 0.5;
	//float scatteringFactor = HenyeyGreenstein(1);

	float hgDivFactor = HenyeyGreenstein(1);

	// RAY MARCH
	float4 col = float4(0, 0, 0, 0);

	//float slope = (dot(outDir, float3(0, 1, 0)) + 0.5) / 1.5;

	//float scale = 3000;
	//int stepCount = 128;// +int(64 * (1 - stepSlope));
	float3 stepDir = outDir;
	//float smallStepSize = (2500.0 / 64.0) / slope;
	//float bigStepSize = (2500 / 32.0) / slope;
	//float currentStepSize = bigStepSize;

	float3 top = IntersectAtmosphere(ViewPos, outDir);
	float dAtm = length(top - input.FragPos);
	float countMod = (dAtm - CloudHeight) / (AtmoshpereSizeAtHorizon - CloudHeight);
	float stepCount = 64 + int(48 * countMod);
	float smallStepSize = dAtm / stepCount;
	//bigStepSize = smallStepSize * 2.0;
	//debugCol = stepCount / 128.0;
	float currentStepSize = smallStepSize;

	float3 windOffset = WindSpeed * Time * 500 * normalize(WindDir);

	float3 stepCoords = input.FragPos.xyz;
	float3 texStepCoords = stepCoords / TxScaleMain;
	float2 seed = float2(dot(texStepCoords.xy, input.Pos.xy), dot(texStepCoords.yz, input.Pos.xy));
	//float rnd_b = RandomZeroOneFloat(seed);
	float rnd_b = 0;
	stepCoords += smallStepSize * rnd_b * 0.1;


	float extinction = 1;
	//float3 scattering = 0;

	[loop] for (int step = 0; step < stepCount; ++step)
	{
		float altitude = GetAltitude(stepCoords);
		float normAltitude = altitude / CloudHeight;

		[branch] if (altitude > CloudHeight)
		{
			//debugCol = float4(0, 1.0, 0, 1);
			break;
		}

		//float rnd_b = 0;
		//float rnd = txCloudDetail.Sample(samLinear, input.FragPos) * 0.4;

		//float3 stepWindOffset = 0;
		float3 stepWindOffset = windOffset;// *saturate(SetRange(normAltitude, 0.1, 1.0, 0.0, 0.8));

		float3 texCoordsHigh = (stepCoords.xyz + stepWindOffset + (rnd_b - 0.5) * currentStepSize) / TxScaleMain; //* 0.0003;
		//float3 texCoordsHigh = (stepCoords.xyz + stepWindOffset + (rnd_b - 0.5) * currentStepSize) / (6000 - 5000 * dot(float3(0,1,0),outDir)); //* 0.0003;
		float4 stepValues = txCloudMain.Sample(samLinear, texCoordsHigh);

		float2 coverageCoords = (stepCoords.xz + TxCoverageOffset) / TxCoverageScale;
		//float2 coverageCoords = (stepCoords.xz - stepWindOffset * normAltitude + TxCoverageOffset) / TxCoverageScale;
		float4 weather = txStaticCoverage.Sample(clampedSamLinear, coverageCoords);

		float distMod = DistanceModifier(stepCoords);

		float localDensity = BaseCloudShape(stepValues, normAltitude, weather);

		float model = localDensity;

		float4 detailValues = txCloudDetail.Sample(samLinear, texCoordsHigh * 4);
		float erMod = (normAltitude + 0.1) * 0.05;
		float erVal = saturate(1 - detailValues.r);
		model = saturate(SetRange(model, erVal * erMod, 1.0, 0.0, 1.0));
		//model = saturate(SetRange(model, (1 - detailValues.g) * erMod * 0.4, 1.0, 0.0, 1.0));

		model = saturate(SetRange(model, (1 - detailValues.b) * erMod, 1.0, 0.0, 1.0));

		[branch] if (model <= 0.001)
		{
			/*[flatten] if (inCloud)
			{
				outOfCloudSteps += 1;
			}*/
			stepCoords += stepDir * currentStepSize;
			continue;
		}
		//[branch] if (!inCloud)
		//{
		//	inCloud = true;
		//	outOfCloudSteps = 0;
			/*stepCoords = stepCoords - stepDir * currentStepSize;
			currentStepSize = smallStepSize;
			cloudStart = stepCoords;
			// We don't want this iteration to contirbute 1 to light
			stepCoords += stepDir * currentStepSize;
			continue;*/
		//}
		/*[flatten] if (outOfCloudSteps >= 5)
		{
			inCloud = false;
			currentStepSize = bigStepSize * 4;
			inCloudSteps = 0;
		}*/

		//float2 seed = float2(dot(texStepCoords.xy, input.Pos.xy), dot(texStepCoords.yz, input.Pos.xy));
		//float rnd_b = RandomZeroOneFloatTime(seed, Time);

		//float scatteringCoeff = scatteringFactor * (1 - model);
		//float extinctionCoeff = extinctionFactor * (1 - model) * 0.03;
		float extinctionCoeff = extinctionFactor * model;

		//float2 seed = float2(dot(texStepCoords.xy, input.Pos.xy), dot(texStepCoords.yz, input.Pos.xy));
		//float rnd_b = RandomZeroOneFloat(seed) / 2 + 0.5;

		extinction *= exp(-extinctionCoeff * currentStepSize);
		//extinction *= exp(-extinctionCoeff * currentStepSize) * (1 - model);

		//float powder = 1.0 - exp(-currentStepSize * (1 - model) * 0.05 * 2);
		//float powder = 1.0 - exp(-currentStepSize * 0.05) * (1 - model);
		float powder = 1.0 - exp(-currentStepSize * model);
		//float powder = 1.0;

		//float dist = length(stepCoords - cloudStart);
		//float energy = exp(-dist / scale) * (1 - model);
		float cosAngle = dot(-stepDir, normalize(-DirLights[0].Direction));
		float HG = HenyeyGreenstein(-cosAngle);
		HG = HG / hgDivFactor;
		//float powder = 1;
		//float powder = 1.0 - exp(-dist / scale * 2);// *clamp((cosAngle + 1) / 4.0, 0, 1);

		float shade = 1;
		float3 txCoords = stepCoords / (TxScaleMain * 10);
		float3 shadeCoords = stepCoords;
		[loop] for (int i = 0; i < 6; ++i)
		{
			float offset = i == 6 ? 15 * smallStepSize : 3 * smallStepSize;
			
			shadeCoords += lightDir * offset;
			float shadeAltitude = GetAltitude(shadeCoords);
			float normShadeAltitude = shadeAltitude / CloudHeight;
			float2 shadeCoverageCoords = (shadeCoords.xz + TxCoverageOffset) / TxCoverageScale;
			float4 shadeCoverage = txStaticCoverage.Sample(clampedSamLinear, shadeCoverageCoords);
			[branch] if (shadeAltitude > CloudHeight)
			{
				break;
			}
			txCoords += (lightDir * offset) / (TxScaleMain * (i + 10));
			float4 shadeValues = txCloudMain.Sample(samLinear, txCoords);
			float shadeDensity = BaseCloudShape(shadeValues, normShadeAltitude, shadeCoverage);
			
			//shade *= exp(-offset * (1 - shadeDensity) * shadeDensity * 0.05);
			//shade *= exp(-offset * shadeDensity * 0.0175);// *(1 - shadeDensity);
			shade *= exp(-offset * extinctionFactor * shadeDensity);

			//shadeCoords += (shadeValues.xyz - 0.5) * currentStepSize * i;
		}

		//float mcol = max(max(col.r, col.g), col.b);
		//float3 sunColor = sunCol.rgb;
		//sunColor *= energy * HG * powder;

		//float alpha = (1 - col.a) * model + col.a;
		float alpha = (1 - extinction);

		float lightReduction = shade * extinction * HG;
		//float3 ambient = lightReduction * (1 / FourPI) * skyCol * 0.25 * normAltitude;
		float3 ambient = lightReduction * ambientSkyCol * 0.25 * normAltitude;
		//float3 ambient = 0;
		//float angle = dot(-stepDir, float3(0, -1, 0));
		//col = float4(col.rgb + /*(1 - mcol) **/ sunColor * (1 - shade), alpha);
		//col = float4(col.rgb + sunCol.rgb * shade * extinction * scatteringFactor * (1.0 - exp(-currentStepSize * extinctionCoeff * 2)), alpha);
		//col = float4(col.rgb + sunCol.rgb * (1 - weather.b) * shade * extinction * HG * powder * 0.08, alpha);
		col = float4(col.rgb + sunCol.rgb * (1 - weather.b) * lightReduction * powder * 0.08 + ambient, alpha);
		//col += (1-col)*float3(0.5, 0.5, 0.5) * (1 - shade) * (1 - attenuation);

		//if (col.a > 0.95)
		if (extinction < 0.01)
		{
			//debugCol = float4(1.0, 0, 0, 1);
			break;
		}

		stepCoords += stepDir * currentStepSize;
	}

	float distMod = SetRange(DistanceModifier(input.FragPos.xyz), 0.0, 1.0, 0.0, 0.7);
	float4 baseValues = txCloudMain.Sample(samLinear, texStepCoords);
	float3 hazeCol = 0.8 * float3(0.10, 0.10, 0.15);// +0.1 * baseValues.r;
	//float haze = distMod * hazeCol + (1 - distMod) * skyCol;

	//skyCol = skyCol * (1 - col.a) + col;
	//skyCol = extinction * skyCol + col;
	skyCol = float4(hazeCol, 1) * distMod + (extinction * skyCol + col) * (1 - distMod);

	//return debugCol;
	return skyCol;
}
