
#include "CloudsCommon.fx"
#include "../shaders/PhongLights.fx"

Texture3D txCloudMain : register(t0);
Texture3D txCloudDetail : register(t1);
Texture2D txStaticCoverage : register(t2);

SamplerState samLinear : register(s0);
SamplerState clampedSamLinear : register(s1);

cbuffer ConstantBuffer : register(b0) {
	matrix World;
	matrix View;
	matrix Projection;
	matrix NormalMatrix;
	DirLight DirLights[1];
	float3 ViewPos;
	int DirLightCount;
}

struct VS_INPUT {
	float4 Pos : POSITION;
	float3 Norm : NORMAL;
	float2 UV : TEXCOORD0;
};

struct PS_INPUT {
	float4 Pos : SV_POSITION;
	float3 Norm : TEXCOORD1;
	float3 FragPos : POSITION;
	float2 UV : TEXCOORD0;
};

// ===================
// Vertex Shader Plane
// ===================
PS_INPUT VSPlane(VS_INPUT input) {
	PS_INPUT output = (PS_INPUT)0;
	output.Pos = mul(input.Pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Norm = mul(float4(input.Norm, 0), NormalMatrix).xyz;
	output.FragPos = mul(input.Pos, World);
	output.UV = input.UV;

	return output;
}


// ==================
// Pixel shader Plane
// ==================
float4 PSPlane(PS_INPUT input) : SV_Target{
	float3 normal = normalize(input.Norm);
	float3 viewDir = normalize(ViewPos - input.FragPos);

	float4 finalColor = float4(0.0, 0.0, 0.0, 0.0);

	float3 coords = float3(input.FragPos.y / 2000, input.FragPos.x / 2000, input.FragPos.z / 2000);
	float4 texColor = txCloudMain.Sample(samLinear, coords);

	// RAY MARCH
	float4 col = float4(0, 0, 0, 0);

	float2 coverageCoords = (input.FragPos.xz + 5000) / 10000;

	float4 coverage = txStaticCoverage.Sample(samLinear, coverageCoords);

	float stepSlope = (dot(-viewDir, float3(0, 1, 0)) + 0.0) / 1.0;

	float scale = 3000;
	int stepCount = 128;// +int(64 * (1 - stepSlope));
	float3 stepDir = -viewDir.xyz;
	float smallStepSize = (2500.0 / 64.0);
	float bigStepSize = (2500 / 32.0);
	float currentStepSize = bigStepSize;
	//float3 stepCoords = float3(input.FragPos.x / scale, input.FragPos.y / scale, input.FragPos.z / scale);
	float3 stepCoords = input.FragPos.xyz;
	float rnd = (sin(0.79531 * input.FragPos.x + 4.3269 - 46.32169 * input.FragPos.z - 4.2369) + 1.0) / 2.0;
	stepCoords += smallStepSize * rnd * 0.2;
	float valueMod = 1;
	float attenuation = 0.0;
	bool bigStep = true;
	float4 debugCol = float4(0, 0, 0, 1);
	bool inCloud = false;
	float3 cloudStart = float3(0, 0, 0);
	int outOfCloudSteps = 0;
	[loop] for (int step = 0; step < stepCount; ++step)
	{
		if (stepCoords.y > input.FragPos.y + 2500)
		{
			//debugCol = float4(0, 1.0, 0, 1);
			break;
		}

		float3 texCoordsHigh = stepCoords.xyz / float3(scale, scale, scale);
		float3 texCoordsLow = stepCoords.xyz / float3(scale / 2, scale / 2, scale / 2);
		float4 stepValues = txCloudMain.Sample(samLinear, texCoordsHigh);

		float2 coverageCoords = (stepCoords.xz + 5000) / 10000; // step coords is yxz
		float4 coverage = txStaticCoverage.Sample(samLinear, coverageCoords);

		float distMod = DistanceModifier(stepCoords);

		//float cvg = clamp(coverage + distMod, 0, 1);
		float totalCoverage = (coverage.r + distMod); //Can be more than 1

		float localDensity = stepValues.r * AltitudeModifier(stepCoords.y - input.FragPos.y) * CloudSelector(totalCoverage, stepCoords.y - input.FragPos.y);

		// Edit local density
		localDensity = clamp(localDensity - stepValues.g * 0.3, 0, 1);

		float densityModel = totalCoverage * localDensity;

		//float model = clamp(stepValues.r - 0.5, 0, 1) / 0.5;
		//float cloudAbs = (model) * AltitudeModifier(stepCoords.y - input.FragPos.y) * valueMod * CloudSelector(totalCoverage, stepCoords.y - input.FragPos.y);// *coverage.r;

		float model = densityModel;

		// Edit model
		//model = clamp(model - stepValues.g * 0.5, 0, 1);

		if (inCloud)
		{
			//float4 detailValues = txCloudDetail.Sample(samLinear, texCoords);
			//cloudAbs = clamp(cloudAbs - detailValues.g * 0.03, 0, 1);
		}
		//debugCol = float4(cloudAbs, 0, 0, 1);

		if (model == 0)
		{
			if (inCloud)
			{
				outOfCloudSteps += 1;
			}
			stepCoords += stepDir * currentStepSize;
			continue;
		}
		if (model > 0 && !inCloud)
		{
			inCloud = true;
			outOfCloudSteps = 0;
			stepCoords = stepCoords - stepDir * currentStepSize;
			currentStepSize = smallStepSize;
			cloudStart = stepCoords;
			// We don't want this iteration to contirbute 1 to light
			stepCoords += stepDir * currentStepSize;
			continue;
		}
		if (outOfCloudSteps >= 5)
		{
			inCloud = false;
			currentStepSize = bigStepSize;
		}

		float dist = length(stepCoords - cloudStart);
		//float energy = 1;
		//float energy = exp(-dist * model * 5);
		float energy = exp(-dist * 0.005);
		float powder = 1;
		//float powder = 1.0 * exp(-dist * 2);
		float HG = 1;
		//float HG = HenyeyGreenstein(1);

		/*if (cloudAbs <= 0.05)
		{
			if (!bigStep)
			{
				bigStep = true;
				currentStepSize = bigStepSize;
			}
			continue;
		}
		if (cloudAbs > 0.05 && bigStep)
		{
			bigStep = false;
			stepCoords -= stepDir * currentStepSize;
			currentStepSize = smallStepSize;
			continue;
		}*/

		//attenuation += (1 - attenuation) * stepAtt;
		//attenuation += (1 - attenuation) * cloudAbs;

		float shade = 0;
		if (energy > 0.25)
			//if (attenuation < 0.3 && attenuation > 0.05)
			{
			//float4 shadeValues = txCloudMain.Sample(samLinear, stepCoords - normalize(DirLights[0].Direction) * bigStepSize);
			//shade = shadeValues.r;
			//shadeValues = txCloudMain.Sample(samLinear, stepCoords - normalize(DirLights[0].Direction) * 2 * bigStepSize);
			//shade = (1 - shade) * shadeValues.r;
		}

		float3 light = DirLights[0].Color.rgb;
		light *= energy * HG * powder;

		float alpha = (1 - col.a) * model;

		col = float4(col.rgb + light * (1 - shade), alpha);
		//col += (1-col)*float3(0.5, 0.5, 0.5) * (1 - shade) * (1 - attenuation);

		if (col.a > 0.95)
		{
			//debugCol = float4(1.0, 0, 0, 1);
			break;
		}

		stepCoords += stepDir * currentStepSize;
		debugCol = col.a;
	}

	//attenuation = (attenuation + DistanceModifier(input.FragPos.xyz, float3(0, 0, 0))) / 2.0;
	//attenuation = clamp(attenuation + DistanceModifier(input.FragPos.xyz, float3(0, 0, 0)), 0, 1);

	//finalColor = saturate(finalColor);

	//float4 skyCol = float4(0.2, 0.2, dot(viewDir, float4(0, -1, 0, 0)), 1.0);
	//float4 skyCol = float4(0.1, 0.1, (0.2 + dot(viewDir, float4(0, -1, 0, 0))) / 1.2, 1.0);
	float3 lightDir = normalize(-DirLights[0].Direction);
	float dotlight = dot(lightDir, viewDir);
	float sunlight = pow(max(dot(-viewDir, lightDir), 0.0), 150);
	float4 skyCol = float4(0.1, 0.1, (0.4 + dot(viewDir, float4(0, -1, 0, 0))) / 1.4, 1.0);
	float4 sunCol = float4(0.85, 0.85, 0.7, 1.0);
	float4 resCol = sunlight * sunCol + (1 - sunlight) * skyCol;
	//resCol = float4(0, 0.7, 0, 1);
	//resCol = resCol * (1 - attenuation) + float4(0.8, 0.8, 0.8, 1) * attenuation;
	//resCol = resCol * (1 - attenuation) + float4(col.rgb, 1) * attenuation;
	resCol = resCol * (1 - col.a) + col;
	//debugCol = float4(1.0, 0, 0, 1);
	//resCol = float4(1.0, 0, 0, 1);
	//resCol = resCol * (attenuation) + float4(col.rgb, 1) * (1 - attenuation);
	//resCol = resCol * (texColor.r) + float4(0.8, 0.8, 0.8, 1) * (1 - texColor.r);
	//resCol = float4(texColor.r, 0, 0, 1);
	//resCol = float4(attenuation, 0, 0, 1);
	//debugCol = txStaticCoverage.Sample(samLinear, coverageCoords);
	//resCol = float4(debugCol.r, 0, 0, 1);

	//resCol = float4(input.FragPos.xyz / 9000, 1);

	/*float3 texCoords = float3(0.0, input.FragPos.x, input.FragPos.z) / float3(1, scale / 1, scale / 1);
	float3 texCoords2 = float3(input.FragPos.x, 0.0, input.FragPos.z) / float3(scale / 1, 1, scale / 1);
	float3 texCoords3 = float3(input.FragPos.x, input.FragPos.z, 0.0) / float3(scale / 1, scale / 1, 1);
	float4 stepValues = txCloudMain.Sample(samLinear, texCoords);
	float4 stepValues2 = txCloudMain.Sample(samLinear, texCoords2);
	float4 stepValues3 = txCloudMain.Sample(samLinear, texCoords3);
	resCol = float4(0, stepValues3.r, 0, 1);*/

	//return skyCol;
	return resCol;
	//return debugCol;
}
/*float4 PSPlane(PS_INPUT input) : SV_Target{
	float3 normal = normalize(input.Norm);
	float3 viewDir = normalize(ViewPos - input.FragPos);
	//float4 fragColor = txDiffuse.Sample(samLinear, input.UV);

	float4 finalColor = float4(0.0, 0.0, 0.0, 0.0);

	//float3 coords = float3(input.FragPos.x / 16000, input.FragPos.y / 16000, input.FragPos.z / 16000);
	//float3 coords = float3(input.FragPos.x / 400, 0.5, input.FragPos.z / 4000);
	float3 coords = float3(0.5, input.FragPos.x / 4000, input.FragPos.z / 4000);
	//float3 coords = float3(input.FragPos.x / 400, input.FragPos.z / 4000, 0.5);
	float4 texColor = txCloudMain.Sample(samLinear, coords);

	// RAY MARCH
	float3 col = float3(0, 0, 0);

	float scale = 8000;
	int stepCount = 8;
	float3 stepDir = -viewDir.yxz;
	float smallStepSize = (2000.0 / 32.0) / (scale);
	float bigStepSize = (2000 / 128.0) / scale;
	//float3 stepCoords = float3(input.FragPos.x / scale, input.FragPos.y / scale, input.FragPos.z / scale);
	float3 stepCoords = float3(0.0, input.FragPos.x / scale, input.FragPos.z / scale);
	float valueMod = 0.35;
	float attenuation = 0.0;
	for (int step = 0; step < stepCount; ++step)
	{
		float4 stepValues = txCloudMain.Sample(samLinear, stepCoords);

		//float cloudAbs = clamp(stepValues.r - 0.5, 0, 1);
		float cloudAbs = stepValues.r * 0.5 * valueMod;

		if (cloudAbs <= 0.05)
		{
			continue;
		}

		float stepSize = smallStepSize;
		//float stepAtt = (stepValues.r - stepValues.g * 0.5 - stepValues.b * 0.3 - stepValues.a * 0.2) * valueMod;
		//stepAtt = clamp(stepAtt, 0, 1);

		//attenuation += (1 - attenuation) * stepAtt;
		attenuation += (1 - attenuation) * cloudAbs;

		float4 shadeValues = txCloudMain.Sample(samLinear, stepCoords - normalize(DirLights[0].Direction) * bigStepSize);
		float shade = shadeValues.r;
		shadeValues = txCloudMain.Sample(samLinear, stepCoords - normalize(DirLights[0].Direction) * 2 * bigStepSize);
		shade = (1 - shade) * shadeValues.r;


		col += float3(0.8, 0.8, 0.8) * (1 - shade) * (1 - attenuation);

		if (attenuation > 0.95)
		{
			break;
		}

		stepCoords += stepDir * stepSize;
	}

	//attenuation = (attenuation + DistanceModifier(input.FragPos.xyz, float3(0, 0, 0))) / 2.0;
	attenuation = clamp(attenuation + DistanceModifier(input.FragPos.xyz, float3(0, 0, 0)), 0, 1);

	//finalColor = saturate(finalColor);

	//float4 skyCol = float4(0.2, 0.2, dot(viewDir, float4(0, -1, 0, 0)), 1.0);
	//float4 skyCol = float4(0.1, 0.1, (0.2 + dot(viewDir, float4(0, -1, 0, 0))) / 1.2, 1.0);
	float3 lightDir = normalize(-DirLights[0].Direction);
	float dotlight = dot(lightDir, viewDir);
	float sunlight = pow(max(dot(-viewDir, lightDir), 0.0), 75);
	float4 skyCol = float4(0.15, 0.1, (0.2 + dot(viewDir, float4(0, -1, 0, 0))) / 1.2, 1.0);
	float4 sunCol = float4(0.85, 0.85, 0.7, 1.0);
	float4 resCol = sunlight * sunCol + (1 - sunlight) * skyCol;

	//resCol = resCol * (1 - attenuation) + float4(0.8, 0.8, 0.8, 1) * attenuation;
	resCol = resCol * (1 - attenuation) + float4(col.rgb, 1) * attenuation;
	//resCol = resCol * (attenuation) + float4(col.rgb, 1) * (1 - attenuation);
	//resCol = resCol * (texColor.r) + float4(0.8, 0.8, 0.8, 1) * (1 - texColor.r);

	//resCol = float4(input.FragPos.xyz / 9000, 1);

	//return skyCol;
	return resCol;
	//return finalColor;
	//return float4(normal.x, normal.y, normal.z, 1.0);
}*/