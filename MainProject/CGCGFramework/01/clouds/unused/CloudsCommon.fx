
static const float PI = 3.14159265f;
static const float TwoPI = 2 * PI;
static const float FourPI = 4 * PI;
static const float PI2 = PI * PI;
static const float HG_G = 0.25;
static const float CloudHeight = 2500;
static const float BaseDensityIncreaseHeight = 400;
static const float BaseAltitudeReducer = 0.5;
static const float BaseAltitudeDivider = 1 + BaseAltitudeReducer;
static const float AtmoshpereSizeAtHorizon = 13500;
static const float2 StratusGradient = float2(0.1, 0.3);
static const float2 CumulusGradient = float2(0.1, 1.0);
static const float2 CumulonimbusGradient = float2(0.75, 1.0);
static const float TxScaleMain = 4000;
static const float TxScaleDetail = 500;
static const float HorizonDensityBegin = 15000;
static const float HorizonDensityEnd = 22500;
static const float HorizonDensityDelta = HorizonDensityEnd - HorizonDensityBegin;
static const float AtmosphereRadius = 150000;
static const float TxCoverageOffset = 22000;
static const float TxCoverageScale = 44000;
static const float SunDistance = 250000;

static const float3 EarthCenter = float3(0, -148500, 0);

float RandomZeroOneFloat(float2 seed)
{
	return frac(sin(dot(seed.xy, float2(12.9898, 78.233))) * 43758.5453);
}

float RandomZeroOneFloatTime(float2 seed, float time)
{
	return frac(sin(dot(seed.xy, float2(12.9898, 78.233) * (frac(time) + 1))) * 43758.5453);
}

float GetAltitude(float3 pos)
{
	return length(pos - EarthCenter) - AtmosphereRadius;
}

/// Returns 0-1 value based on the distance from the centre.
float DistanceModifier(float3 position)
{
	float d = length(position.xz - EarthCenter.xz);
	return saturate((d - HorizonDensityBegin) / HorizonDensityDelta);
}

/// Rescales value from one range into another.
float SetRange(float value, float oldStart, float oldEnd, float newStart, float newEnd)
{
	return ((value - oldStart) / (oldEnd - oldStart)) * (newEnd - newStart) + newStart;
}

float StratusSelector(float alt)
{
	return smoothstep(50, 150, alt) - smoothstep(200, 300, alt);
}

float CumulusSelector(float alt)
{
	return smoothstep(50, 200, alt) - smoothstep(600, 750, alt);
}

float CumulonimbusSelector(float alt)
{
	return smoothstep(50, 500, alt) - smoothstep(1500, 2250, alt);
}

float CloudSelectorByCoverage(float coverage, float alt)
{
	float d = 200;
	alt = alt / CloudHeight;
	float max = coverage * 2000;
	float min = 0;
	float a = smoothstep(0, d, alt);
	float b = smoothstep(max - d, max, alt);
	return (a + (1 - b)) / 2;
}

float CloudSelector(float coverage, float alt)
{
	return CloudSelectorByCoverage(coverage, alt);
}

float AltitudeModifier(float alt)
{
	//return clamp(alt / CloudHeight, 0, 1) + (1 - clamp(alt / 300, 0, 1)) * 0.3;
	return (saturate(alt / CloudHeight) + BaseAltitudeReducer) / BaseAltitudeDivider;
}

float ErosionModifier(float alt)
{
	return saturate(alt / CloudHeight * 2);
}

float2 GetCloudGradient(float cloudType)
{
	float a = 1.0 - saturate(cloudType * 2.0);
	float b = 1.0 - abs(cloudType - 0.5) * 2.0;
	float c = saturate(cloudType - 0.5) * 2.0;

	return (StratusGradient * a) + (CumulusGradient * b) + (CumulonimbusGradient * c);
}

float GetCoverageErosion(float normAltitude, float coverage)
{
	return saturate(coverage - normAltitude * 0.1);
}

float GetAltitudeDensity(float normAltitude, float density)
{
	return saturate((normAltitude * 2 + 0.1) * density);
}

float GetAltitudeCoverage(float normAltitude, float2 gradient) {
	float coverage1 = saturate(normAltitude / 0.1);
	float coverage2 = 1.0 - saturate((normAltitude - gradient.x) / (gradient.y - gradient.x));
	return coverage1 * coverage2;
}

float BaseDensityIncrease(float alt)
{
	float baseAlt = saturate((BaseDensityIncreaseHeight - alt) / BaseDensityIncreaseHeight);
	return lerp(1.0, 3.0, baseAlt);
}

float HenyeyGreenstein(float cosAngle)
{
	return (1 - HG_G * HG_G) / (FourPI * pow(1 + HG_G * HG_G - 2 * HG_G * cosAngle, 3 / 2));
}

float Ei(float z) {
	return 0.5772156649015328606065 + log(1e-4 + abs(z)) + z * (1.0 + z * (0.25 + z * ((1.0 / 18.0) + z * ((1.0 / 96.0) + z * (1.0 / 600.0))))); // For z!=0
}

float3 SolveQuadratic(float a, float b, float c)
{
	float discr = b * b - 4 * a * c;
	[flatten]if (discr < 0) return float3(-1, 0, 0);
	float x0 = 0;
	[flatten] if (discr == 0)
	{
		x0 = -0.5 * b / a;
		return float3(1, x0, x0);
	}
	float x1 = 0;
	float q = (b > 0) ?
		-0.5 * (b + sqrt(discr)) :
		-0.5 * (b - sqrt(discr));
	x0 = q / a;
	x1 = c / q;

	return float3(1, max(x0, x1), min(x0, x1));
}

float3 IntersectAtmosphere(float3 start, float3 dir)
{
	float3 L = start - EarthCenter;
	float radius = (AtmosphereRadius + CloudHeight);
	float a = dot(dir, dir);
	float b = 2 * dot(dir, L);
	float c = dot(L, L) - radius * radius;
	float3 res = SolveQuadratic(a, b, c);
	return start + res.y * dir;
}

float MainCloudShape(float4 noiseTex, float normAltitude, float2 gradient, float coverage)
{
	
	float altMod = GetAltitudeCoverage(normAltitude, gradient);


	float modelA = noiseTex.r * 0.6 + noiseTex.b * 0.3;

	float modelC = saturate(SetRange(modelA, 0.0, 1.0, noiseTex.b, 1.0));
	float cwc = saturate(SetRange(modelA, coverage, 1.0, 0.0, 1.0));
	coverage = pow(coverage, SetRange(normAltitude, 0.7, 0.8, 1.0, lerp(1.0, 0.5, 0.05)));

	float areClouds = coverage.r > 0 ? 1.0 : 0.0;
	float localDensity = GetAltitudeDensity(normAltitude, modelC) * altMod * coverage;// *totalCoverage * 2;// * coverage.r * 2;
	localDensity = saturate(localDensity);

	return localDensity;
}

float BaseCloudShape(float4 noiseTex, float normAltitude, float4 weather)
{
	float bottomCoverage = (1 - saturate(normAltitude * 4.0)) * 0.5;
	float topCoverage = saturate(SetRange(normAltitude, 0.7, 0.9, 0.0, 1.0));
	float coverage = weather.r;
	float2 cloudGrad = GetCloudGradient(weather.g * weather.r + 0.15);
	float altMod = GetAltitudeCoverage(normAltitude * 0.85 + bottomCoverage * weather.g, cloudGrad);
	coverage += topCoverage * coverage;

	float density = saturate(SetRange(noiseTex.r * 0.6 + noiseTex.g * 0.4, 0.05, 1.0, 0.0, 1.0));
	density = saturate(SetRange(density, 0.0, 1.0, noiseTex.b * 0.1, 1.0));
	density = saturate(SetRange(density, noiseTex.b * 0.1, 1.0, noiseTex.a * 0.1, 1.0));

	return density * altMod * coverage;
}