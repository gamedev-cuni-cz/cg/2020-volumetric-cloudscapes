
#include "CloudTextureLoader.hpp"

#include <fstream>
#include <iostream>

using namespace Clouds;
using namespace DirectX;

CloudTextureDescriptor CloudTextureLoader::Load3DTexture(const std::string& pathNoExt)
{
	return Load3DTexture(pathNoExt + ".desc", pathNoExt + ".raw");
}

CloudTextureDescriptor CloudTextureLoader::Load3DTexture(const std::string& pathToHeader, const std::string& pathToData)
{
	CloudTextureDescriptor ctd;

	std::ifstream header;
	header.open(pathToHeader, std::ios::in);
	int32_t sizeX, sizeY, sizeZ;
	header >> sizeX;
	header >> sizeY;
	header >> sizeZ;
	header.close();
	std::cout << "Resolution of 3D texture: " << sizeX << " " << sizeY << " " << sizeZ << std::endl;

	ctd.sizeX = sizeX;
	ctd.sizeY = sizeY;
	ctd.sizeZ = sizeZ;

	std::vector<float> data;

	std::ifstream datafile;
	datafile.open(pathToData, std::ios::in | std::ios::binary);

	//char value[4];
	float fvalue;
	if (datafile.is_open())
	{
		while (!datafile.eof()) {
			//datafile.read(value, 4);
			datafile.read((char*)&fvalue, sizeof(float));
			//std::cout << value[0] << value[1] << value[2] << value[3] << std::endl;
			//float val(float(std::atof(value)));
			float val = fvalue;
			//std::cout << val << std::endl;
			/*if (val != 0)
			{
				std::cout << val << std::endl;
			}*/
			data.push_back(val);
		}
		datafile.close();
	}
	else std::cout << "Unable to open file " << pathToData;

	ctd.data = std::move(data);

	/*for (int i = 0; i < 10; i++)
	{
		std::cout << ctd.data[2648 + i] << std::endl;
	}*/

	return ctd;
}

HRESULT CloudTextureLoader::CreateTexture(ID3D11Device* device, const CloudTextureDescriptor& descriptor, ID3D11ShaderResourceView** resourceView)
{
	HRESULT hr;
	D3D11_TEXTURE3D_DESC desc;
	desc.Width = descriptor.sizeX;
	desc.Height = descriptor.sizeY;
	desc.Depth = descriptor.sizeZ;
	desc.MipLevels = 1;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;
	
	D3D11_SUBRESOURCE_DATA data;
	data.pSysMem = descriptor.data.data();
	data.SysMemPitch = descriptor.sizeX * 16;
	data.SysMemSlicePitch = descriptor.sizeX * descriptor.sizeY * 16;

	ID3D11Texture3D* texture = nullptr;

	hr = device->CreateTexture3D(&desc, &data, &texture);

	if (FAILED(hr))
	{
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	memset(&srvDesc, 0, sizeof(srvDesc));
	srvDesc.Format = desc.Format;

	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
	srvDesc.Texture3D.MipLevels = desc.MipLevels;

	hr = device->CreateShaderResourceView(texture, &srvDesc, resourceView);

	return S_OK;
}
