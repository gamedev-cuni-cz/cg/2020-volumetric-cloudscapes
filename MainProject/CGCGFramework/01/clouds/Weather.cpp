
#include "Weather.hpp"

#include "../src/DDSTextureLoader.h"

using namespace Clouds;
using namespace DirectX;

HRESULT CoverageTextureLoader::LoadCoverageTexture(ID3D11Device* device, const std::wstring& filepath, ID3D11ShaderResourceView** textureView)
{
	HRESULT hr = CreateDDSTextureFromFile(device, filepath.c_str(), true, nullptr, textureView);
	return hr;
}