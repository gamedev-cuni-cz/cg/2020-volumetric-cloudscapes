#pragma once

#include <string>
#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>

namespace Clouds
{
	struct CloudTextureDescriptor
	{
		int32_t sizeX;
		int32_t sizeY;
		int32_t sizeZ;
		std::vector<float> data;
	};

	class CloudTextureLoader
	{
	public:
		CloudTextureDescriptor Load3DTexture(const std::string& pathNoExt);
		CloudTextureDescriptor Load3DTexture(const std::string& pathToHeader, const std::string& pathToData);

		HRESULT CreateTexture(ID3D11Device* device, const CloudTextureDescriptor& descriptor, ID3D11ShaderResourceView** resourceView);
	};
}