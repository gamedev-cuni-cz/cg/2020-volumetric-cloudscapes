#pragma once

#include "../src/BaseExample.h"

#include "../src/PhongLights.h"
#include "../src/ShaderProgram.h"
#include "../src/TexturedCube.h"
#include "../src/ColorCube.h"
#include "../src/Plane.h"

#include "SkyGeometry.hpp"
#include <chrono>

namespace Clouds {

	struct ConstantBuffer {
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMMATRIX NormalMatrix;
		DirLight DirLights[1];
		DirectX::XMFLOAT3 ViewPos;
		int DirLightCount;
	};

	struct CloudConstantBuffer
	{
		DirectX::XMMATRIX World;
		DirectX::XMMATRIX View;
		DirectX::XMMATRIX Projection;
		DirectX::XMMATRIX NormalMatrix;
		DirLight DirLights[1];
		DirectX::XMFLOAT3 ViewPos;
		int DirLightCount;
		float Time;
		DirectX::XMFLOAT3 WindDir;
		float WindSpeed;
	};

	class CloudsExample : public BaseExample {
	protected:
		using TextureShader = ShaderProgram<ConstantBuffer>;
		using CloudShader = ShaderProgram<CloudConstantBuffer>;

		ID3D11ShaderResourceView* seaFloorTexture_ = nullptr;
		ID3D11ShaderResourceView* boxTexture_ = nullptr;
		ID3D11SamplerState* textureSampler_ = nullptr;

		std::unique_ptr<TextureShader> texturedPhong_;
		std::unique_ptr<TexturedCube> texturedCube_;
		std::unique_ptr<Plane> texturedPlane_;
		Shaders::PSolidShader solidShader_;
		std::unique_ptr<ColorCube> colorCube_;

		ID3D11ShaderResourceView* mainCloudTexture_ = nullptr;
		ID3D11ShaderResourceView* detailCloudTexture_ = nullptr;
		ID3D11ShaderResourceView* staticCoverageTexture_ = nullptr;
		ID3D11SamplerState* clampedTextureSampler_ = nullptr;

		std::unique_ptr<CloudShader> cloudPlaneShader_;
		std::unique_ptr<CloudShader> cloudSphereShader_;
		std::unique_ptr<SkyPlaneGenerator> skyPlane_;
		std::unique_ptr<SkySphereGenerator> skySphere_;

		std::chrono::system_clock::time_point oldTime_;
		bool firstRender_ = true;
		double totalSeconds_ = 0.0;

		HRESULT setup() override;
		bool reloadShadersInternal() override;
		void render() override;
	};
}
