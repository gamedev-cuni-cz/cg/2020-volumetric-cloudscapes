#pragma once

#include <string>
#include <vector>
#include <d3d11.h>
#include <DirectXMath.h>

namespace Clouds
{
	class CoverageTextureLoader
	{
	public:
		HRESULT LoadCoverageTexture(ID3D11Device* device, const std::wstring& filepath, ID3D11ShaderResourceView** textureView);
	private:
	};
}