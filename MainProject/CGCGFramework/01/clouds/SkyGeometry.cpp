
#include "SkyGeometry.hpp"

#include <iostream>

using namespace Clouds;
using namespace DirectX;

SkyPlaneGenerator::SkyPlaneGenerator(ID3D11Device* device, XMFLOAT3 position, XMFLOAT2 size, bool invertedNormals)
	: position_(position), size_(size), invertedNormals_(invertedNormals)
{
	Generate();
	initialize(device, vertices_, indices_);
}

void SkyPlaneGenerator::Generate()
{
	XMFLOAT2 sizeHalf = XMFLOAT2(size_.x / 2, size_.y / 2);
	float normalY = 1;
	if (invertedNormals_) { normalY = -1; }
	// Vertex (-1, -1)
	vertexPositions_.emplace_back(XMFLOAT3(position_.x - sizeHalf.x, position_.y, position_.z - sizeHalf.y));
	vertexNormal_.emplace_back(XMFLOAT3(0, normalY, 0));
	vertexUV_.emplace_back(XMFLOAT2(0, 0));
	// Vertex (-1, 1)
	vertexPositions_.emplace_back(XMFLOAT3(position_.x - sizeHalf.x, position_.y, position_.z + sizeHalf.y));
	vertexNormal_.emplace_back(XMFLOAT3(0, normalY, 0));
	vertexUV_.emplace_back(XMFLOAT2(0, 1));
	// Vertex (1, 1)
	vertexPositions_.emplace_back(XMFLOAT3(position_.x + sizeHalf.x, position_.y, position_.z + sizeHalf.y));
	vertexNormal_.emplace_back(XMFLOAT3(0, normalY, 0));
	vertexUV_.emplace_back(XMFLOAT2(1, 1));
	// Vertex (1, -1)
	vertexPositions_.emplace_back(XMFLOAT3(position_.x + sizeHalf.x, position_.y, position_.z - sizeHalf.y));
	vertexNormal_.emplace_back(XMFLOAT3(0, normalY, 0));
	vertexUV_.emplace_back(XMFLOAT2(1, 0));

	for (size_t i = 0; i < vertexPositions_.size(); ++i)
	{
		vertices_.emplace_back(SkyPlaneVertex{ vertexPositions_[i], vertexNormal_[i], vertexUV_[i] });
	}
	indices_ = { 0, 1, 2, 0, 2, 3 };
}

VertexLayout_t SkyPlaneGenerator::getVertexLayout() const
{
	return SKY_PLANE_LAYOUT;
}

SkySphereGenerator::SkySphereGenerator(
	ID3D11Device* device,
	XMFLOAT3 position,
	float radius,
	int32_t latResolution,
	int32_t longResolution,
	bool halfSphere,
	int32_t numRings,
	bool invertedNormals
) :
	position_(position),
	radius_(radius),
	latResolution_(latResolution),
	longResolution_(longResolution),
	halfSphere_(halfSphere),
	numRings_(numRings),
	invertedNormals_(invertedNormals)
{
	Generate();
	initialize(device, vertices_, indices_);
}

void SkySphereGenerator::Generate()
{
	double fullLatAngle = XM_PI;
	double dLatAngle = fullLatAngle / (latResolution_);
	double dLongAngle = XM_2PI / longResolution_;

	double currentLat = dLatAngle / 2;

	float defNormal = 1;
	if (invertedNormals_)
	{
		defNormal = -1;
	}

	// Top vertex
	vertexPositions_.emplace_back(XMFLOAT3(position_.x, position_.y + radius_, position_.z));
	vertexNormal_.emplace_back(XMFLOAT3(0, defNormal, 0));
	vertexLatLong_.emplace_back(XMFLOAT2(0, 0));

	size_t latCount = latResolution_;
	if (halfSphere_)
	{
		//latCount = latCount / 2 + 1;
		latCount = numRings_;
	}
	for (size_t latIdx = 0; latIdx < latCount; ++latIdx)
	{
		double currentLong = 0.0;
		for (size_t longIdx = 0; longIdx < longResolution_; ++longIdx)
		{
			float r = float(sin(currentLat));
			float y = float(cos(currentLat)); // vertical
			float x = float(r * cos(currentLong)); // horizontal
			float z = float(r * sin(currentLong)); // horizontal

			vertexPositions_.emplace_back(XMFLOAT3(
				position_.x + x * radius_,
				position_.y + y * radius_,
				position_.z + z * radius_
			));
			XMFLOAT3 n(
				vertexPositions_.back().x - position_.x,
				vertexPositions_.back().y - position_.y,
				vertexPositions_.back().z - position_.z
			);
			if (invertedNormals_)
			{
				n = XMFLOAT3(-n.x, -n.y, -n.z);
			}
			XMStoreFloat3(&n, XMVector3Normalize(XMLoadFloat3(&n)));
			vertexNormal_.emplace_back(n);
			vertexLatLong_.emplace_back(XMFLOAT2(float(currentLat), float(currentLong)));

			currentLong += dLongAngle;
		}
		currentLat += dLatAngle;
	}
	if (!halfSphere_)
	{
		// Bottom vertex
		vertexPositions_.emplace_back(XMFLOAT3(position_.x, position_.y - radius_, position_.z));
		vertexNormal_.emplace_back(XMFLOAT3(0, -defNormal, 0));
		vertexLatLong_.emplace_back(XMFLOAT2(XM_PI, 0));
	}

	for (size_t i = 0; i < vertexPositions_.size(); ++i)
	{
		vertices_.emplace_back(SkySphereVertex{ vertexPositions_[i], vertexNormal_[i], vertexLatLong_[i] });
	}
	// Top triangles
	for (size_t i = 0; i < longResolution_ - 1; ++i)
	{
		indices_.push_back(1 + uint32_t(i));
		indices_.push_back(0);
		indices_.push_back(2 + uint32_t(i));
	}
	indices_.push_back(longResolution_);
	indices_.push_back(0);
	indices_.push_back(1);
	// Ring triangles
	uint32_t ringCurrent = 1;
	for (size_t i = 0; i < latCount - 1; ++i)
	{
		for (size_t j = 0; j < longResolution_ - 1; ++j)
		{
			indices_.push_back(ringCurrent);
			indices_.push_back(ringCurrent + 1);
			indices_.push_back(ringCurrent + longResolution_ + 1);

			indices_.push_back(ringCurrent);
			indices_.push_back(ringCurrent + longResolution_ + 1);
			indices_.push_back(ringCurrent + longResolution_);

			ringCurrent++;
		}
		indices_.push_back(ringCurrent);
		indices_.push_back(ringCurrent - longResolution_ + 1);
		indices_.push_back(ringCurrent + 1);

		indices_.push_back(ringCurrent);
		indices_.push_back(ringCurrent + 1);
		indices_.push_back(ringCurrent + longResolution_);

		ringCurrent++;
	}
	// Bottom triangles
	if (!halfSphere_)
	{
		uint32_t last = uint32_t(vertices_.size() - 1);
		for (size_t i = 0; i < longResolution_ - 1; ++i)
		{
			indices_.push_back(last);
			indices_.push_back(last - uint32_t(i) - 2);
			indices_.push_back(last - uint32_t(i) - 1);
		}
		indices_.push_back(last);
		indices_.push_back(last - 1);
		indices_.push_back(last - longResolution_);
	}

	/*for (size_t i = 0; i < vertices_.size(); ++i)
	{
		std::cout << vertices_[i].Pos.x << " " << vertices_[i].Pos.y << " " << vertices_[i].Pos.z << std::endl;
		//std::cout << vertices_[i].Normal.x << " " << vertices_[i].Normal.y << " " << vertices_[i].Normal.z << std::endl;
	}

	for (size_t i = 0; i < indices_.size(); i += 3)
	{
		std::cout << indices_[i] << " " << indices_[i + 1] << " " << indices_[i + 2] << std::endl;
	}*/
}

VertexLayout_t SkySphereGenerator::getVertexLayout() const
{
	return SKY_SPHERE_LAYOUT;
}
