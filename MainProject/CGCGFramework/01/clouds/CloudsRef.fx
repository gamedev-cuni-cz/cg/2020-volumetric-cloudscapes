float2 get_cloud_gradient(float cloud_type, float fade_term) {
	float type = cloud_type;//max(cloud_type, fade_term);

	float a = 1.0 - saturate(type / 0.5);
	float b = 1.0 - abs(type - 0.5) * 2.0;
	float c = saturate(type - 0.5) / 0.5;

	return (gradient_stratocumulus * a) + (gradient_cumulus * b) + (gradient_cumulonimbus * c);
}

float get_altitude_coverage(float altitude, float2 gradient) {
	// For now we hardcode the lower part of the gradient as [0.0, 0.05]. This
	// gives acceptable results and saves a lot of opps when ray marching!
	float coverage1 = saturate(altitude / 0.1);
	float coverage2 = 1.0 - saturate((altitude - gradient.x) / (gradient.y - gradient.x));
	return coverage1 * coverage2;
}

float get_fade_term(float3 sample_pos) {
	float distance = length(sample_pos.xy);
	return saturate((distance - 5000) / 20000.0);
}

float get_low_res_cloud(float4 texture1, float altitude, float altitude_scalar, float2 gradient_values, float view_dependent_lerp, float cloud_coverage, float fade_term, float erode_low) {
	
	float altitude_coverage = get_altitude_coverage(altitude, gradient_values);

	// Here we add coverage but only at the bottoms of the clouds to get a pancake 
	// look for distant clouds
	float extra_coverage = (1.0 - saturate(altitude * altitude_scalar)) * 0.5;
	extra_coverage = lerp(0, extra_coverage, pow(view_dependent_lerp, 2));

	float a = 1.0 - fade_term;
	float c = min(cloud_coverage, a);

	float low_range = lerp(1.0, c * COVERAGE_SCALAR + COVERAGE_OFFSET - extra_coverage, altitude_coverage);
	low_range = saturate(low_range);

	float clouds_value = set_range_clamped(texture1.r, low_range, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture1.g * erode_low, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture1.b * erode_low, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture1.a * erode_low, 1.0);

	return clouds_value;
}

float get_altitude_density(float altitude, float density, float cloud_type, float density_end_pos) {
	return saturate(altitude / 0.5 + 0.1) * density;
}

float get_high_res_cloud(Sampler3D tex, float3 clouds_sampling_coord, float low_res_clouds_data, float3 curl_offset, float altitude, float view_dependent_lerp, float density, float cloud_type, float density_end_pos, float erode_high) {
	density *= 0.1;
	float4 texture2 = TEX3DLOD(tex, clouds_sampling_coord * clouds_advance_erode_scale + curl_offset, 0);
	texture2 = lerp(texture2, 1.0 - texture2, saturate((altitude - 0.4) * 5));

	float altitude_density = get_altitude_density(altitude, density, cloud_type, density_end_pos);

	float clouds_value = low_res_clouds_data.r;
	clouds_value = set_range_clamped(clouds_value, texture2.r * erode_high * 1.0, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture2.g * erode_high * 0.8, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture2.b * erode_high * 0.6, 1.0);

	clouds_value = clouds_value * altitude_density;

	return clouds_value;
}

float4 get_final_color(float3 scattering, float extinction, float blend_color_scalar, float blend_amount, float bottom_darkening, float extinction_altitude, float altitude_scalar, float3 extinction_color, float bottom_darkening_start) {
	// We darken the extinction color at the base of the clouds (and randomize it a bit to avoid a flat gradient look)
	float dark_bottoms = extinction_altitude * altitude_scalar - bottom_darkening_start;
	float b = lerp(blend_color_scalar * bottom_darkening, blend_color_scalar, saturate(dark_bottoms));

	float3 final_color = scattering + extinction_color * b;

	// Add an offset where there is high luminance of the scattering term (so we can keep nice cloud highlights)
	float scattering_blend_offset = luminance(scattering) * clouds_advance_scattering_color_blend;
	float alpha = (1.0 - extinction) * saturate(blend_amount + scattering_blend_offset);

	// From this point onwards, all other passes can assume alpha is in a [0,1] range
	return float4(final_color, saturate(alpha));
}

float get_altitude_scalar(float cloud_type) {
	return lerp(8.0, 2.0, cloud_type);
}

float2 get_clouds_erosion(float coverage) {
	float2 s = saturate(coverage / 0.2 + float2(0, 0.5));
	return clouds_advance_erode * s;
}

float get_high_res_cloud(Sampler3D tex, float3 clouds_sampling_coord, float low_res_clouds_data, float3 curl_offset, float altitude, float view_dependent_lerp, float density, float cloud_type, float density_end_pos, float erode_high) {
	density *= 0.1;
	float4 texture2 = TEX3DLOD(tex, clouds_sampling_coord * clouds_advance_erode_scale + curl_offset, 0);
	texture2 = lerp(texture2, 1.0 - texture2, saturate((altitude - 0.4) * 5));

	float altitude_density = get_altitude_density(altitude, density, cloud_type, density_end_pos);

	float clouds_value = low_res_clouds_data.r;
	clouds_value = set_range_clamped(clouds_value, texture2.r * erode_high * 1.0, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture2.g * erode_high * 0.8, 1.0);
	clouds_value = set_range_clamped(clouds_value, texture2.b * erode_high * 0.6, 1.0);

	clouds_value = clouds_value * altitude_density;

	return clouds_value;
}