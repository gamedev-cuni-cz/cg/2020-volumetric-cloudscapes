
#include "Clouds.hpp"
#include "CloudTextureLoader.hpp"
#include "Weather.hpp"
#include "../src/DDSTextureLoader.h"

#include <directxcolors.h>

namespace Clouds {

	using namespace DirectX;

	HRESULT CloudsExample::setup() {
		auto hr = BaseExample::setup();
		if (FAILED(hr))
			return hr;

		hr = reloadShaders();
		if (FAILED(hr))
			return hr;

		// new projection
		projection_ = DirectX::XMMatrixPerspectiveFovLH(XM_PIDIV4, context_.WIDTH / static_cast<FLOAT>(context_.HEIGHT), 0.01f, 200000.0f);

		texturedCube_ = std::make_unique<TexturedCube>(context_.d3dDevice_);
		texturedPlane_ = std::make_unique<Plane>(context_.d3dDevice_);
		solidShader_ = Shaders::createSolidShader(context_);
		colorCube_ = std::make_unique<ColorCube>(context_.d3dDevice_);

		//skyPlane_ = std::make_unique<SkyPlaneGenerator>(context_.d3dDevice_);
		skyPlane_ = std::make_unique<SkyPlaneGenerator>(
			context_.d3dDevice_,
			DirectX::XMFLOAT3(0, 500, 0),
			DirectX::XMFLOAT2(10000, 10000),
			true
		);
		//skySphere_ = std::make_unique<SkySphereGenerator>(context_.d3dDevice_);
		skySphere_ = std::make_unique<SkySphereGenerator>(
			context_.d3dDevice_,
			DirectX::XMFLOAT3(0, -148500, 0),
			150000.0f,
			127,
			256,
			true,
			8,
			true
		);

		// load cloud 3D textures
		CloudTextureLoader ctl;
		CloudTextureDescriptor mainCloudDesc = ctl.Load3DTexture("clouds/data/clouds_v001");
		CloudTextureDescriptor detailCloudDesc = ctl.Load3DTexture("clouds/data/detail_v001");
		//CloudTextureDescriptor detailCloudDesc = ctl.Load3DTexture("clouds/data/detailNONE");

		hr = ctl.CreateTexture(context_.d3dDevice_, mainCloudDesc, &mainCloudTexture_);
		if (FAILED(hr))
			return hr;
		hr = ctl.CreateTexture(context_.d3dDevice_, detailCloudDesc, &detailCloudTexture_);
		if (FAILED(hr))
			return hr;

		CoverageTextureLoader covtl;
		//=======================================================================================================================
		// Here you can change the weather map used in the application.
		//=======================================================================================================================
		// Reasonable cloudscapes are in weather maps 06 - 09.
		// Weather_06.dds ... Storm wall, also from presentation, it has black underside because the clouds are very thick
		// Weather_07.dds ... Cloudy weather, some of the clouds have pillar shape because the map doesn't have gradual decrease
		//					  around the cloud.
		// Weather_08.dds ... Mostly clear sky with some clouds here and there.
		// Weather_09.dds ... Other storm wall which does not have completely black underside but it is similar to 06.
		// Weather_10.dds ... Cloudy weather with fairly thin clouds - visible repetition and texture artefacts.
		hr = covtl.LoadCoverageTexture(context_.d3dDevice_, L"clouds/data/Weather_06.dds", &staticCoverageTexture_);

		if (FAILED(hr))
			return hr;

		std::cout << "LOADED TEXTURE" << std::endl;

		// ========================
		// Create sea floor texture
		// ========================
		hr = CreateDDSTextureFromFile(context_.d3dDevice_, L"textures/seafloor.dds", true, nullptr, &seaFloorTexture_);

		/* Uncomment following to allow mipmap generation */
		//hr = CreateDDSTextureFromFile(context_.d3dDevice_, context_.immediateContext_, L"textures/seafloor.dds", nullptr, &seaFloorTexture_);

		if (FAILED(hr))
			return hr;


		// =======================
		// Create box wood texture
		// =======================
		hr = CreateDDSTextureFromFile(context_.d3dDevice_, L"textures/container2.dds", true, nullptr, &boxTexture_);

		/* Uncomment following to allow mipmap generation */
		//hr = CreateDDSTextureFromFile(context_.d3dDevice_, context_.immediateContext_, L"textures/seafloor.dds", nullptr, &seaFloorTexture_);

		if (FAILED(hr))
			return hr;


		// ======================
		// Create texture sampler
		// ======================
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

		/* Uncomment following to enable point filtering */
		//sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;

		/* Uncomment following to eable anisotropic filtering */
		//sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		//sampDesc.MaxAnisotropy = 16;

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		hr = context_.d3dDevice_->CreateSamplerState(&sampDesc, &textureSampler_);
		if (FAILED(hr))
			return hr;

		// ======================
		// Create clamped texture sampler
		// ======================
		D3D11_SAMPLER_DESC clampSampDesc;
		ZeroMemory(&clampSampDesc, sizeof(clampSampDesc));
		clampSampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

		//clampSampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		//clampSampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		//clampSampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		clampSampDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		clampSampDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		clampSampDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		FLOAT borderCol[4] = { 0.f, 0.f, 0.f, 1.f };
		clampSampDesc.BorderColor[0] = 0.f;
		clampSampDesc.BorderColor[1] = 0.f;
		clampSampDesc.BorderColor[2] = 0.f;
		clampSampDesc.BorderColor[3] = 1.f;
		clampSampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		clampSampDesc.MinLOD = 0;
		clampSampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		hr = context_.d3dDevice_->CreateSamplerState(&clampSampDesc, &clampedTextureSampler_);
		if (FAILED(hr))
			return hr;

		totalSeconds_ = 0.0;

		return S_OK;
	}

	bool CloudsExample::reloadShadersInternal() {
		std::vector<D3D11_INPUT_ELEMENT_DESC> texturedLayout = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		bool textureShaderResult = Shaders::makeShader<TextureShader>(texturedPhong_, context_.d3dDevice_, L"shaders/Textured.fx", "VS", L"shaders/Textured.fx", "PS", texturedLayout);
		bool cloudShaderSphereResult = Shaders::makeShader<CloudShader>(cloudSphereShader_, context_.d3dDevice_, L"clouds/CloudsSphere.fx", "VSSphere", L"clouds/CloudsSphere.fx", "PSSphere", SKY_SPHERE_LAYOUT);
		return textureShaderResult && cloudShaderSphereResult;
	}

	void CloudsExample::render() {
		BaseExample::render();

		double seconds = 0.0;
		auto newTime = std::chrono::system_clock::now();
		if (!firstRender_)
		{
			std::chrono::duration<double> delta = newTime - oldTime_;
			seconds = delta.count();
			std::cout << 1.0 / seconds << std::endl;
			//std::cout << "t" << std::endl;
		}
		oldTime_ = newTime;
		//std::cout << seconds << std::endl;
		totalSeconds_ += seconds;
		firstRender_ = false;
		//std::cout << totalSeconds_ << std::endl;

		context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, Util::srgbToLinear(DirectX::Colors::Black));
		context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

		
		// =========
		// Draw cube
		// =========
		XMFLOAT4 sunPos = XMFLOAT4(-30.0f, 30.0f, -30.0f, 1.0f);

		XMFLOAT4 cubePos = XMFLOAT4(0.0, 2.0f, 0.0f, 1.0f);
		ConstantBuffer cb;
		//cb.World = XMMatrixIdentity();
		cb.World = XMMatrixTranspose(XMMatrixTranslationFromVector(XMLoadFloat4(&cubePos)));
		cb.NormalMatrix = computeNormalMatrix(cb.World);
		cb.Projection = XMMatrixTranspose(projection_);
		cb.View = XMMatrixTranspose(camera_.getViewMatrix());
		cb.ViewPos = camera_.Position;
		cb.DirLightCount = 1;
		cb.DirLights[0].Color = SUN_YELLOW;
		cb.DirLights[0].Direction = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);

		texturedPhong_->updateConstantBuffer(context_.immediateContext_, cb);
		texturedPhong_->use(context_.immediateContext_);
		context_.immediateContext_->PSSetShaderResources(0, 1, &boxTexture_);
		context_.immediateContext_->PSSetSamplers(0, 1, &textureSampler_);
		texturedCube_->draw(context_.immediateContext_);

		// ==========
		// Draw floor
		// ==========
		XMFLOAT4 planePos = XMFLOAT4(0.0, 0.0f, 0.0f, 1.0f);
		const XMMATRIX planeScale = XMMatrixScaling(120.0f, 0.2f, 120.0f);
		cb.World = XMMatrixTranspose(planeScale * XMMatrixTranslationFromVector(XMLoadFloat4(&planePos)));
		cb.NormalMatrix = computeNormalMatrix(cb.World);

		texturedPhong_->updateConstantBuffer(context_.immediateContext_, cb);
		context_.immediateContext_->PSSetShaderResources(0, 1, &seaFloorTexture_);
		context_.immediateContext_->PSSetSamplers(0, 1, &textureSampler_);
		texturedPlane_->draw(context_.immediateContext_);

		//============
		// Draw sphere
		//============
		D3D11_RASTERIZER_DESC CurrentRasterizerState;
		CurrentRasterizerState.FillMode = D3D11_FILL_SOLID;
		CurrentRasterizerState.CullMode = D3D11_CULL_FRONT;
		CurrentRasterizerState.FrontCounterClockwise = false;
		CurrentRasterizerState.DepthBias = false;
		CurrentRasterizerState.DepthBiasClamp = 0;
		CurrentRasterizerState.SlopeScaledDepthBias = 0;
		CurrentRasterizerState.DepthClipEnable = true;
		CurrentRasterizerState.ScissorEnable = false;
		CurrentRasterizerState.MultisampleEnable = true;
		CurrentRasterizerState.AntialiasedLineEnable = false;

		ID3D11RasterizerState* state = nullptr;
		HRESULT hr = context_.d3dDevice_->CreateRasterizerState(&CurrentRasterizerState, &state);
		if (FAILED(hr)) {
			MessageBox(nullptr, L"Failed to create rasterizer state", L"Error", MB_OK);
		}

		ID3D11RasterizerState* oldstate = nullptr;
		context_.immediateContext_->RSGetState(&oldstate);

		context_.immediateContext_->RSSetState(state);

		XMFLOAT4 spherePos = XMFLOAT4(0.0, 0.0f, 0.0f, 1.0f);

		CloudConstantBuffer cbs;
		cbs.World = XMMatrixTranspose(XMMatrixTranslationFromVector(XMLoadFloat4(&spherePos)));
		//cbs.NormalMatrix = XMMatrixIdentity();
		cbs.NormalMatrix = computeNormalMatrix(cbs.World);
		cbs.Projection = XMMatrixTranspose(projection_);
		cbs.View = XMMatrixTranspose(camera_.getViewMatrix());
		cbs.ViewPos = camera_.Position;
		cbs.DirLightCount = 1;
		//cbs.DirLights[0].Color = SUN_YELLOW;
		//=======================================================================================================================
		// Here you can change colour and direction of the sun
		//=======================================================================================================================
		cbs.DirLights[0].Color = XMFLOAT4(0.96f, 0.93f, 0.93f, 1.0f);
		//cbs.DirLights[0].Color = XMFLOAT4(0.76f, 0.23f, 0.23f, 1.0f);
		cbs.DirLights[0].Direction = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);
		//cbs.DirLights[0].Direction = XMFLOAT4(0.9, -0.3, -0.9, 1.0f);
		cbs.Time = float(totalSeconds_);
		cbs.WindDir = XMFLOAT3(0.13f, 0.0f, -1.0f);
		//cbs.WindDir = XMFLOAT3(-1.01f, 0.0f, 0.0f);
		cbs.WindSpeed = 0.24f;

		cloudSphereShader_->updateConstantBuffer(context_.immediateContext_, cbs);
		cloudSphereShader_->use(context_.immediateContext_);
		context_.immediateContext_->PSSetShaderResources(0, 1, &mainCloudTexture_);
		context_.immediateContext_->PSSetShaderResources(1, 1, &detailCloudTexture_);
		context_.immediateContext_->PSSetShaderResources(2, 1, &staticCoverageTexture_);
		context_.immediateContext_->PSSetSamplers(0, 1, &textureSampler_);
		context_.immediateContext_->PSSetSamplers(1, 1, &clampedTextureSampler_);
		skySphere_->draw(context_.immediateContext_);

		context_.immediateContext_->RSSetState(oldstate);

		context_.swapChain_->Present(0, 0);
	}
}

// Hack to force the application on the dedicated GPU
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}