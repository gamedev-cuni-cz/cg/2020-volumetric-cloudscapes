
#include "CloudsCommon.fx"
#include "../shaders/PhongLights.fx"

Texture3D txCloudMain : register(t0);
Texture3D txCloudDetail : register(t1);
Texture2D txStaticCoverage : register(t2);

SamplerState samLinear : register(s0);
SamplerState clampedSamLinear : register(s1);

cbuffer ConstantBuffer : register(b0) {
	matrix World;
	matrix View;
	matrix Projection;
	matrix NormalMatrix;
	DirLight DirLights[1];
	float3 ViewPos;
	int DirLightCount;
	float Time;
	float3 WindDir;
	float WindSpeed;
}


struct VS_INPUT_SPHERE
{
	float4 Pos : POSITION;
	float3 Norm : NORMAL;
	float2 UV : LATLONG;
};

struct PS_INPUT_SPHERE
{
	float4 Pos : SV_POSITION;
	float3 Norm : TEXCOORD1;
	float3 FragPos : POSITION;
	float2 UV : LATLONG;
};


// ====================
// Vertex Shader Sphere
// ====================
PS_INPUT_SPHERE VSSphere(VS_INPUT_SPHERE input) {
	PS_INPUT_SPHERE output = (PS_INPUT_SPHERE)0;
	output.Pos = mul(input.Pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Norm = mul(float4(input.Norm, 0), NormalMatrix).xyz;
	output.FragPos = mul(input.Pos, World);
	output.UV = input.UV;

	return output;
}


// ===================
// Pixel shader Sphere
// ===================
float4 PSSphere(PS_INPUT_SPHERE input) : SV_Target{
	float3 normal = normalize(input.Norm);
	float3 viewDir = normalize(ViewPos - input.FragPos);
	float3 outDir = -viewDir;

	float4 debugCol = float4(0, 0, 0, 1);

	float3 sunPos = normalize(DirLights[0].Direction) * SunDistance;

	float3 lightDir = normalize(input.FragPos - sunPos);
	float sunlight = pow(max(dot(outDir, lightDir), 0.0), 150);
	float4 bgCol = float4(0.1, 0.1, (0.4 + dot(viewDir, float4(0, -1, 0, 0))) / 1.4, 1.0);
	float4 sunCol = DirLights[0].Color;
	float4 skyCol = sunlight * sunCol + (1 - sunlight) * bgCol;

	float3 ambientSkyCol = skyCol.rgb / FourPI;
	float3 ambientSunCol = DirLights[0].Color / FourPI;
	//float extinctionFactor = 0.04;
	float extinctionFactor = 0.02;
	//float extinctionFactor = 0.005;

	float hgDivFactor = HenyeyGreenstein(1);

	float4 col = float4(0, 0, 0, 0);

	float3 stepDir = outDir;

	float3 top = IntersectAtmosphere(ViewPos, outDir);
	float dAtm = length(top - input.FragPos);
	float countMod = (dAtm - CloudHeight) / (AtmoshpereSizeAtHorizon - CloudHeight);
	float stepCount = 64 + int(48 * countMod);
	float smallStepSize = dAtm / stepCount;
	float currentStepSize = smallStepSize;

	float3 windOffset = WindSpeed * Time * 500 * normalize(WindDir);

	float3 stepCoords = input.FragPos.xyz;
	float3 texStepCoords = stepCoords / TxScaleMain;
	float2 seed = float2(dot(texStepCoords.xy, input.Pos.xy), dot(texStepCoords.yz, input.Pos.xy));
	//float rnd_b = RandomZeroOneFloat(seed);
	float rnd_b = 0;
	stepCoords += smallStepSize * rnd_b * 0.1;


	float extinction = 1;

	[loop] for (int step = 0; step < stepCount; ++step)
	{
		float altitude = GetAltitude(stepCoords);
		float normAltitude = altitude / CloudHeight;

		[branch] if (altitude > CloudHeight)
		{
			break;
		}

		//float3 stepWindOffset = 0;
		float3 stepWindOffset = windOffset;

		// Adding noise to the texture coordinates to reduce woodgrain artefacts - pretty big performance hit
		//float2 seed = float2(dot(texStepCoords.xy, input.Pos.xy), dot(texStepCoords.yz, input.Pos.xy));
		//rnd_b = RandomZeroOneFloat(seed);
		//float3 texCoordsHigh = (stepCoords.xyz + stepWindOffset + (rnd_b - 0.5) * currentStepSize) / TxScaleMain;

		float3 texCoordsHigh = (stepCoords.xyz + stepWindOffset) / TxScaleMain;
		float4 stepValues = txCloudMain.Sample(samLinear, texCoordsHigh);

		float2 coverageCoords = (stepCoords.xz + TxCoverageOffset) / TxCoverageScale;
		// Experiment with increasing wind speed with altitude - produced artefacts
		//float2 coverageCoords = (stepCoords.xz - stepWindOffset * normAltitude + TxCoverageOffset) / TxCoverageScale;
		float4 weather = txStaticCoverage.Sample(clampedSamLinear, coverageCoords);

		float model = BaseCloudShape(stepValues, normAltitude, weather);

		float4 detailValues = txCloudDetail.Sample(samLinear, texCoordsHigh * 4);
		float erMod = (normAltitude + 0.1) * 0.05;
		float erVal = saturate(1 - detailValues.r);
		model = saturate(SetRange(model, erVal * erMod, 1.0, 0.0, 1.0));
		//model = saturate(SetRange(model, (1 - detailValues.g) * erMod * 0.4, 1.0, 0.0, 1.0));
		model = saturate(SetRange(model, (1 - detailValues.b) * erMod, 1.0, 0.0, 1.0));

		[branch] if (model <= 0.001)
		{
			stepCoords += stepDir * currentStepSize;
			continue;
		}

		float extinctionCoeff = extinctionFactor * model;

		extinction *= exp(-extinctionCoeff * currentStepSize);

		// Powder term to darken creases
		float powder = 1.0 - exp(-currentStepSize * model);
		//float powder = 1.0;

		// Forward light scattering
		float cosAngle = dot(-stepDir, normalize(-DirLights[0].Direction));
		float HG = HenyeyGreenstein(-cosAngle);
		HG = HG / hgDivFactor;

		// Light marching for self shadows
		float shade = 1;
		float3 txCoords = stepCoords / (TxScaleMain * 10);
		float3 shadeCoords = stepCoords;
		[loop] for (int i = 0; i < 6; ++i)
		{
			float offset = i == 6 ? 15 * smallStepSize : 3 * smallStepSize;
			
			shadeCoords += lightDir * offset;
			float shadeAltitude = GetAltitude(shadeCoords);
			float normShadeAltitude = shadeAltitude / CloudHeight;
			float2 shadeCoverageCoords = (shadeCoords.xz + TxCoverageOffset) / TxCoverageScale;
			float4 shadeCoverage = txStaticCoverage.Sample(clampedSamLinear, shadeCoverageCoords);
			[branch] if (shadeAltitude > CloudHeight)
			{
				break;
			}
			txCoords = shadeCoords / (TxScaleMain * (i + 10));
			float4 shadeValues = txCloudMain.Sample(samLinear, txCoords);
			float shadeDensity = BaseCloudShape(shadeValues, normShadeAltitude, shadeCoverage);
			
			// Artificial constant which reduces the shading applied by clouds
			// - The clouds seemed too dark.
			shade *= exp(-offset * extinctionFactor * 0.35 * shadeDensity);
		}

		// Compute alpha
		float alpha = (1 - extinction);

		// Compute colour
		float lightReduction = shade * extinction * HG;
		float3 ambient = lightReduction * ambientSkyCol * 0.25 * normAltitude;

		col = float4(col.rgb + sunCol.rgb * (1 - weather.b) * lightReduction * powder * 0.08 + ambient, alpha);

		// Early exit based on extinction
		if (extinction < 0.01)
		{
			break;
		}

		stepCoords += stepDir * currentStepSize;
	}

	// Add haze at the horizon
	float distMod = SetRange(DistanceModifier(input.FragPos.xyz), 0.0, 1.0, 0.0, 0.7);
	float4 baseValues = txCloudMain.Sample(samLinear, texStepCoords);
	float3 hazeCol = 0.8 * float3(0.10, 0.10, 0.15);

	skyCol = float4(hazeCol, 1) * distMod + (extinction * skyCol + col) * (1 - distMod);

	return skyCol;
}
