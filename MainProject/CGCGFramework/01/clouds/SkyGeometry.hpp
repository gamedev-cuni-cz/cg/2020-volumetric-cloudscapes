#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>
#include "../src/DrawableObject.h"

namespace Clouds
{
	using VertexLayout_t = std::vector<D3D11_INPUT_ELEMENT_DESC>;

	struct SkyPlaneVertex
	{
		DirectX::XMFLOAT3 Pos;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 UV;
	};

	const VertexLayout_t SKY_PLANE_LAYOUT = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	struct SkySphereVertex
	{
		DirectX::XMFLOAT3 Pos;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 LatLong;
	};

	const VertexLayout_t SKY_SPHERE_LAYOUT = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "LATLONG", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	class SkyPlaneGenerator : public DrawableObject<SkyPlaneVertex>
	{
	public:
		SkyPlaneGenerator(
			ID3D11Device* device,
			DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0, 0, 0),
			DirectX::XMFLOAT2 size = DirectX::XMFLOAT2(1, 1),
			bool invertedNormals = true
		);

		Layouts::VertexLayout_t getVertexLayout() const override;
	private:
		void Generate();

		DirectX::XMFLOAT3 position_;
		DirectX::XMFLOAT2 size_;
		bool invertedNormals_;
		
		// buffers

		std::vector<DirectX::XMFLOAT3> vertexPositions_;
		std::vector<DirectX::XMFLOAT3> vertexNormal_;
		std::vector<DirectX::XMFLOAT2> vertexUV_;

		std::vector<SkyPlaneVertex> vertices_;
		//std::vector<uint32_t> indices_;
		std::vector<WORD> indices_;
	};

	class SkySphereGenerator : public DrawableObject<SkySphereVertex>
	{
	public:
		SkySphereGenerator(
			ID3D11Device* device,
			DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0, 0, 0),
			float radius = 1,
			int32_t latResolution = 31,
			int32_t longResolution = 64,
			bool halfSphere = false,
			int32_t numRings = 15,
			bool invertedNormals = false
		);

		Layouts::VertexLayout_t getVertexLayout() const override;
	private:
		void Generate();

		DirectX::XMFLOAT3 position_;
		float radius_;
		int32_t latResolution_;
		int32_t longResolution_;
		bool halfSphere_;
		int32_t numRings_;
		bool invertedNormals_;

		// buffers

		std::vector<DirectX::XMFLOAT3> vertexPositions_;
		std::vector<DirectX::XMFLOAT3> vertexNormal_;
		std::vector<DirectX::XMFLOAT2> vertexLatLong_;

		std::vector<SkySphereVertex> vertices_;
		//std::vector<uint32_t> indices_;
		std::vector<WORD> indices_;
	};
}